#![crate_name = "easylog"]

//! easylog - An easy and simple to use logger-crate for Rust.
//!
//! easylog let you write logging messages to a file so you can
//! analyse them later.
//!
//! # Example 1:
//! ```rust
//! extern crate easylog;
//!
//! use easylog::log_file::{LogFile, LogLevel};
//! use easylog::log_file_config::LogFileConfig;
//!
//! fn main() {
//!     let default = LogFileConfig::new();
//!     let mut logfile = match LogFile::new(default) {
//!         Ok(file) => file,
//!
//!         Err(error) => {
//!             panic!("Error: `{}`", error);
//!         }
//!     };
//!
//!     logfile.write(LogLevel::DEBUG, "Insert your logmessage here...");
//!     logfile.write(LogLevel::INFO, "Insert your logmessage here...");
//!     logfile.write(LogLevel::WARNING, "Insert your logmessage here...");
//!     logfile.write(LogLevel::ERROR, "Insert your logmessage here...");
//!     logfile.write(LogLevel::CRITICAL, "Insert your logmessage here...");
//! }
//! ```
//!
//! # Example Output:
//! ```text
//! $ cat ./logfile_0.log
//! 2018-06-09 22:51:37.443883  [DEBUG   ]  Insert your logmessage here...
//! 2018-06-09 22:51:37.443969  [INFO    ]  Insert your logmessage here...
//! 2018-06-09 22:51:37.443996  [WARNING ]  Insert your logmessage here...
//! 2018-06-09 22:51:37.444022  [ERROR   ]  Insert your logmessage here...
//! 2018-06-09 22:51:37.444048  [CRITICAL]  Insert your logmessage here...
//! ```
//!
//! # Example 2:
//! ```rust
//! extern crate easylog;
//!
//! use easylog::log_file::{LogFile, LogLevel};
//! use easylog::log_file_config::LogFileConfig;
//!
//! fn main() {
//!     let mut custom_config = LogFileConfig::new();
//!
//!     custom_config.max_size_in_mb = 2;
//!     custom_config.path = String::from("./");    //  String::from("/path/to/logfile/");
//!     custom_config.name = String::from("my_logfile");
//!     custom_config.extension = String::from(".txt");
//!     custom_config.num_files_to_keep = 2;
//!
//!     let logfile = match LogFile::new(custom_config) {
//!         Ok(file) => file,
//!
//!         Err(error) => {
//!             panic!("Error: `{}`", error);
//!         }
//!     };
//! }
//! ```
//!
//! # Example 3
//! ```rust
//! extern crate easylog;
//!
//! use easylog::log_file::{LogFile, LogLevel};
//! use easylog::log_file_config::LogFileConfig;
//!
//! fn main() {
//!     let mut custom_config = LogFileConfig::new();
//!
//!     custom_config.max_size_in_mb = 2;
//!     custom_config.path = String::from("./");    //  String::from("/path/to/logfile/");
//!     custom_config.name = String::from("my_logfile");
//!     custom_config.extension = String::from(".txt");
//!     custom_config.overwrite = false;
//!     custom_config.num_files_to_keep = 1337; // has no effect, because overwrite is false
//!
//!     let logfile = match LogFile::new(custom_config) {
//!         Ok(file) => file,
//!
//!         Err(error) => {
//!             panic!("Error: `{}`", error);
//!         }
//!     };
//! }
//! ```
//!

pub mod log_file;
pub mod log_file_config;
