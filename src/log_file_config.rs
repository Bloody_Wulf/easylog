///
/// Struct to hold the configuration for the logfile
///
#[derive(Clone)]
pub struct LogFileConfig {
    /// maximum size for the logfile in MegaBytes - example: 1, 2, 3, 4, etc.
    pub max_size_in_mb: u64,

    /// Path to the file - example: "./", "~/logfolder/"
    pub path: String,

    /// filename - example: "logfile_", "my_log"
    pub name: String,

    /// fileextension - example: ".log", ".txt"
    pub extension: String,

    /// shall files be overwriten (starting from the first)
    /// when num_files_to_keep is reached.
    pub overwrite: bool,

    /// shall the file be truncated.
    pub truncate: bool,

    /// how many files shall be kept, if overwrite is true.
    /// If overwrite is false, this param has no effect.
    pub num_files_to_keep: u64,
}

impl LogFileConfig {
    /// Returns a LogFileConfig-struct with default parameters
    ///
    /// ## Default Params:
    /// - max_size_in_mb: 1
    /// - path: "./"
    /// - name: "logfile_"
    /// - extension: ".log"
    /// - overwrite: true
    /// - truncate: true
    /// - num_files_to_keep: 5
    ///
    pub fn new() -> LogFileConfig {
        LogFileConfig {
            max_size_in_mb: 1,
            path: String::from("./"),
            name: String::from("logfile_"),
            extension: String::from(".log"),
            overwrite: true,
            truncate: true,
            num_files_to_keep: 5,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn default_constructor_test() {
        let default = LogFileConfig::new();

        assert_eq!(default.max_size_in_mb, 1);
        assert_eq!(default.path, "./");
        assert_eq!(default.name, "logfile_");
        assert_eq!(default.extension, ".log");
        assert_eq!(default.overwrite, true);
        assert_eq!(default.num_files_to_keep, 5);
    }
}
