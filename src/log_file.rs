extern crate chrono;

use std::error::Error;
use std::fs;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::Write;
use std::string::ToString;

use crate::log_file_config::LogFileConfig;

///
/// Represents the needed file attributes
///
struct FileAttr {
    write: bool,
    append: bool,
    create: bool,
    truncate: bool,
}

impl FileAttr {
    ///
    /// Returns a FilleAttr object with default values
    ///
    pub fn new() -> FileAttr {
        FileAttr {
            write: true,
            append: true,
            create: true,
            truncate: false,
        }
    }
}

///
/// Enum to represent the different Loglevel
///
pub enum LogLevel {
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    CRITICAL,
}

///
/// Struct that represents the LogFile and its config
///
pub struct LogFile {
    /// Maximum allowed size for the Logfile in Megabyte (2^20 = 1024*1024)
    max_size: u64,

    /// Holds the configuration for the Logfile
    config: LogFileConfig,

    /// Complete path for the Logfile
    complete_path: String,

    /// The number of the Logfile
    counter: u64,

    /// The file it self
    file: File,
}

impl LogFile {
    ///
    /// Creates a new Logfile.
    ///
    /// # Example 1
    /// ```rust
    /// extern crate easylog;
    ///
    /// use easylog::log_file::{LogFile, LogLevel};
    /// use easylog::log_file_config::LogFileConfig;
    ///
    /// fn main() {
    ///     let default = LogFileConfig::new();
    ///     let logfile = match LogFile::new(default) {
    ///         Ok(file) => file,
    ///
    ///         Err(error) => {
    ///             panic!("Error: `{}`", error);
    ///         }
    ///     };
    /// }
    /// ```
    ///
    /// You can also modify the config-struct
    ///
    /// # Example 2
    /// ```rust
    /// extern crate easylog;
    ///
    /// use easylog::log_file::{LogFile, LogLevel};
    /// use easylog::log_file_config::LogFileConfig;
    ///
    /// fn main() {
    ///     let mut custom_config = LogFileConfig::new();
    ///
    ///     custom_config.max_size_in_mb = 2;
    ///     custom_config.path = String::from("./path/to/logfile/");
    ///     custom_config.name = String::from("my_logfile");
    ///     custom_config.extension = String::from(".txt");
    ///     custom_config.num_files_to_keep = 2;
    ///
    ///     let logfile = match LogFile::new(custom_config) {
    ///         Ok(file) => file,
    ///
    ///         Err(error) => {
    ///             panic!("Error: `{}`", error);
    ///         }
    ///     };
    /// }
    /// ```
    ///
    /// # Example 3
    /// ```rust
    /// extern crate easylog;
    ///
    /// use easylog::log_file::{LogFile, LogLevel};
    /// use easylog::log_file_config::LogFileConfig;
    ///
    /// fn main() {
    ///     let mut custom_config = LogFileConfig::new();
    ///
    ///     custom_config.max_size_in_mb = 2;
    ///     custom_config.path = String::from("./path/to/logfile/");
    ///     custom_config.name = String::from("my_logfile");
    ///     custom_config.extension = String::from(".txt");
    ///     custom_config.overwrite = false;
    ///     custom_config.num_files_to_keep = 1337; // has no effect, because overwrite is false
    ///
    ///     let logfile = match LogFile::new(custom_config) {
    ///         Ok(file) => file,
    ///
    ///         Err(error) => {
    ///             panic!("Error: `{}`", error);
    ///         }
    ///     };
    /// }
    /// ```
    ///
    /// # Details
    /// At first this function checks, if already a logfile (specified
    /// by the config argument) exist.
    /// If a logfile exist the size is checked. If the size is okay
    /// (actual size < max size) the file will be opened. When
    /// the size is not okay (actual size > max size) a new file will
    /// be created.
    ///
    /// If max_size_in_mb is set to 0 (zero) it will be set to 1. So 1 Megabyte
    /// (1 * 1024 * 1024) is the smallest size for a Logfile.
    ///
    /// If the overwrite param is set to true (default) and num_files_to_keep
    /// (default value is 5) is reached, the first logfile will be overwritten
    /// instead of creating a new one. So you will always have only num_files_to_keep
    /// of logfiles. If overwrite is set to false then you will get as much files as
    /// your machine allows.
    ///
    /// If num_files_to_keep is set to 0 (zero) it will be set to 1. So 1 Logfile
    /// is the smallest amount of Logfiles.
    ///
    pub fn new(mut config: LogFileConfig) -> Result<Self, Box<dyn Error>> {
        if config.max_size_in_mb == 0 {
            config.max_size_in_mb = 1;
        }

        const MEGABYTE: u64 = 1024u64 * 1024u64;
        let max_size = config.max_size_in_mb * MEGABYTE;

        let mut counter = 0u64;
        let mut path = assemble_path(&config, counter);

        loop {
            let file_exist = check_if_file_exist(&path);
            if !file_exist {
                break;
            }

            let size_ok = is_size_ok(&path, max_size);
            if size_ok {
                break;
            } else {
                counter += 1;
                path = assemble_path(&config, counter);
            }
        }

        if config.num_files_to_keep == 0 {
            config.num_files_to_keep = 1;
        }

        if config.overwrite && counter > config.num_files_to_keep {
            counter -= config.num_files_to_keep;
        };

        let default_file_attr = FileAttr::new();
        let logfile = open(max_size, config, counter, default_file_attr)?;

        Ok(logfile)
    }

    ///
    /// Write the given message to the logfile.
    ///
    /// # Example
    /// ```rust
    /// extern crate easylog;
    ///
    /// use easylog::log_file::{LogFile, LogLevel};
    /// use easylog::log_file_config::LogFileConfig;
    ///
    /// fn main() {
    ///     let default = LogFileConfig::new();
    ///     let mut logfile = match LogFile::new(default) {
    ///         Ok(file) => file,
    ///
    ///         Err(error) => {
    ///             panic!("Error: `{}`", error);
    ///         }
    ///     };
    ///
    ///     logfile.write(LogLevel::DEBUG, "Insert your logmessage here...");
    /// }
    /// ```
    ///
    /// # Example Output
    /// `2018-06-08 20:23:44.278165  [DEBUG   ]  Insert your logmessage here...`
    ///
    /// # Details
    /// The function will append a newline at the end of the message and insert
    /// a timestamp and the given loglevel at the beginning of the message.
    ///
    /// This function also check if the actual logfilesize is less then the allowed
    /// maximum size.
    ///
    /// If the actual logfilesize is bigger as the allowed maximum, the actual
    /// file will be closed, the filecounter will be incresaesed by 1 and a new
    /// file will be opened.
    ///
    /// After writing the message to the file flush will be called to ensure
    /// that all is written to file.
    ///
    pub fn write(&mut self, level: LogLevel, msg: &str) {
        let log_msg = self.build_log_msg(level, msg);

        let log_size = self.get_logsize();

        if log_size > self.max_size {
            self.rotate();
        }

        self._write(log_msg);
    }

    ///
    /// Returns a clone of the actual object state
    ///
    pub fn clone(&mut self) -> Self {
        LogFile {
            max_size: self.max_size,
            config: self.config.clone(),
            complete_path: self.complete_path.clone(),
            counter: self.counter,
            file: self.file.try_clone().unwrap(),
        }
    }

    ///
    /// Write the given message to the logfile
    ///
    fn _write(&mut self, msg: String) {
        match self.file.write_all(&msg.into_bytes()) {
            Ok(_) => (),
            Err(error) => panic!("panic while writing to file: `{}`", error),
        }
    }

    ///
    /// Rotates the Logfiles.
    ///
    fn rotate(&mut self) {
        let mut file_attr = FileAttr::new();

        if self.config.overwrite {
            if self.counter == self.config.num_files_to_keep - 1 {
                self.counter -= self.config.num_files_to_keep - 1;
            } else {
                self.counter += 1;
            };
            file_attr.append = false;
            file_attr.truncate = self.config.truncate;
        } else {
            self.counter += 1;
        };

        self.complete_path = assemble_path(&self.config, self.counter);

        self.file = match open_file(&self.complete_path, file_attr) {
            Ok(file) => file,

            Err(error) => {
                let msg = format!(
                    "Could not open new log-file `{}`!  Reason: `{}`",
                    self.complete_path, error
                );
                let msg = self.build_log_msg(LogLevel::CRITICAL, &msg);
                self._write(msg);

                panic!("panic while rotating files: `{}`", error);
            }
        };
    }

    ///
    /// Returns the size in Bytes of the actual Logfile
    ///
    fn get_logsize(&self) -> u64 {
        let meta = self.file.metadata().unwrap();

        meta.len()
    }

    ///
    /// Converts a given LogLevel to a pre formatted string that is ready to use.
    ///
    fn get_loglevel_str(&self, level: LogLevel) -> String {
        match level {
            LogLevel::DEBUG => String::from("  [DEBUG   ]  "),
            LogLevel::INFO => String::from("  [INFO    ]  "),
            LogLevel::WARNING => String::from("  [WARNING ]  "),
            LogLevel::ERROR => String::from("  [ERROR   ]  "),
            LogLevel::CRITICAL => String::from("  [CRITICAL]  "),
        }
    }

    ///
    /// Put all pieces of the Logmessage together so it is ready to be written.
    /// timestamp + loglevel + msg + '\n'
    ///
    fn build_log_msg(&self, level: LogLevel, msg: &str) -> String {
        let mut log_msg = String::new();

        let time_as_string = get_actual_timestamp();
        log_msg.push_str(&time_as_string);

        let level_as_string = self.get_loglevel_str(level);
        log_msg.push_str(&level_as_string);

        log_msg.push_str(&msg);
        log_msg.push('\n');

        log_msg
    }
}

///
/// Opens a file and connect it to a LogFile Object.
///
fn open(
    max_size: u64,
    config: LogFileConfig,
    counter: u64,
    file_attr: FileAttr,
) -> Result<LogFile, Box<dyn Error>> {
    let path = assemble_path(&config, counter);
    let f = open_file(&path, file_attr)?;

    Ok(LogFile {
        max_size,
        config,
        complete_path: path,
        counter,
        file: f,
    })
}

///
/// Opens the file it self and set the file options.
///
fn open_file(path: &str, attr: FileAttr) -> Result<File, Box<dyn Error>> {
    let f = OpenOptions::new()
        .write(attr.write)
        .append(attr.append)
        .create(attr.create)
        .truncate(attr.truncate)
        .open(path)?;

    Ok(f)
}

///
/// Returns the actual timestamp in the form of: "2018-06-09 22:51:37.443883"
///
/// https://docs.rs/chrono/0.4.0/chrono/format/strftime/index.html#specifiers
///
fn get_actual_timestamp() -> String {
    let timestamp = chrono::Local::now();

    timestamp.format("%F %T%.6f").to_string()
}

///
/// Checks if a file (specified by the given path) exist without to open the file.
/// Returns true if the file exist, false otherwise.
///
/// https://stackoverflow.com/questions/32384594/how-to-check-whether-a-path-exists/32384768#32384768
///
fn check_if_file_exist(path: &str) -> bool {
    fs::metadata(path).is_ok()
}

///
/// Put the path pieces, given by the LogFileConfig, together and returns it as as String.
///
fn assemble_path(config: &LogFileConfig, counter: u64) -> String {
    let path = format!(
        "{}{}{}{}",
        config.path,
        config.name,
        counter.to_string(),
        config.extension
    );

    path
}

///
/// Checks if the size of the file (specified by path) is smaller then max_size.
/// If the size of the file is smaller then max_size it will return true,
/// false otherwise.
///
fn is_size_ok(path: &str, max_size: u64) -> bool {
    let mut check = false;

    match fs::metadata(path) {
        Ok(meta) => {
            if meta.len() < max_size {
                check = true;
            }
        }

        Err(error) => {
            panic!("Something went wrong while checking size!\n`{}`\n", error);
        }
    };

    check
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn assemble_path_test() {
        let default_conf = LogFileConfig::new();
        let counter = 0u64;
        let path = assemble_path(&default_conf, counter);

        assert_eq!(path, "./logfile_0.log");
    }

    #[test]
    fn file_exist_test() {
        assert_eq!(check_if_file_exist("testfile.txt"), true);
        assert_eq!(check_if_file_exist("none_existing_file.txt"), false);
    }

    #[test]
    fn size_is_ok_test() {
        assert_eq!(is_size_ok("testfile.txt", 30), true);
        assert_eq!(is_size_ok("testfile.txt", 20), false);
    }

    #[test]
    #[should_panic(expected = "Something went wrong while checking size!")]
    fn size_is_not_ok_file_not_exist_test() {
        assert_eq!(is_size_ok("non_existing_file.txt", 20), true);
    }

    #[test]
    fn get_loglevel_str_test() {
        let default = LogFileConfig::new();
        let logfile = match LogFile::new(default) {
            Ok(file) => file,

            Err(error) => {
                panic!("Error: `{}`", error);
            }
        };

        assert_eq!(logfile.get_loglevel_str(LogLevel::DEBUG), "  [DEBUG   ]  ");
        assert_eq!(logfile.get_loglevel_str(LogLevel::INFO), "  [INFO    ]  ");
        assert_eq!(
            logfile.get_loglevel_str(LogLevel::WARNING),
            "  [WARNING ]  "
        );
        assert_eq!(logfile.get_loglevel_str(LogLevel::ERROR), "  [ERROR   ]  ");
        assert_eq!(
            logfile.get_loglevel_str(LogLevel::CRITICAL),
            "  [CRITICAL]  "
        );
    }

    /*
// commented out, becaus it writes 5 MB to the path. maybe not everyone like
// that behavior. so this test will be disabled for the moment
    #[test]
    fn rotating_test() {
        const TO_KEEP: u64 = 3;
        let mut custom_conf = LogFileConfig::new();
        custom_conf.num_files_to_keep = TO_KEEP;

        let mut logfile = match LogFile::new(custom_conf) {
            Ok(file) => file,

            Err(error) => {
                panic!("Error: `{}`", error);
            }
        };

        for _ in 0..200_000 {
            logfile.write(LogLevel::DEBUG, "~(^-^)~");
        }

        assert!(logfile.counter <= TO_KEEP);
    }
*/
}
