use easylog::log_file::{LogFile, LogLevel};
use easylog::log_file_config::LogFileConfig;

fn main() {
    let default = LogFileConfig::new();
    let mut logfile = match LogFile::new(default) {
        Ok(file) => file,

        Err(error) => {
            panic!("Error: `{}`", error);
        }
    };

    for _ in 0..5 {
        logfile.write(LogLevel::DEBUG, "Insert your logmessage here...");
        logfile.write(LogLevel::INFO, "Insert your logmessage here...");
        logfile.write(LogLevel::WARNING, "Insert your logmessage here...");
        logfile.write(LogLevel::ERROR, "Insert your logmessage here...");
        logfile.write(LogLevel::CRITICAL, "Insert your logmessage here...");
    }
}
