.phony: all release debug check run clean test docs docs-all publish update

all: release

release: clean build docs-all

debug: clean build-debug

build:
	cargo build --release

build-debug:
	cargo build

check:
	cargo check

run:
	cargo run

clean:
	rm -rfv ./*logfile*
	cargo clean -v

test: clean
	echo "this is only a Testfile!" > ./testfile.txt
	mkdir -pv ./path/to/logfile

	cargo test

	rm -rfv ./path
	rm -rfv ./testfile.txt
	rm -rfv ./*logfile*


docs:
	cargo doc --no-deps

docs-all:
	cargo doc

publish: clean  docs
	cargo package
	cargo publish

update: update_stage1 test

update_stage1: clean
	rm -rfv Cargo.lock
